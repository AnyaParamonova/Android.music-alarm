package com.anastasia.musicalarm.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Anastasia_Paramonova on 16.04.2017.
 */

public class Alarm {

    public static String SONG_TITLE_EXTRA = "com.anastasia.musicalarm.alarm._SONGTITLE";
    public static String SONG_ID_EXTRA = "com.anastasia.musicalarm.alarm._SONGID";
    private Song song;
    private int wakeHour;
    private int wakeMinute;

    public Alarm(){
    }

    public Alarm(Song song, int wakeHour, int wakeMinute){
        if(song == null){
            throw new NullPointerException("Can't create object Alarm with null parameters.");
        }

        this.song = song;
        this.wakeHour = wakeHour;
        this.wakeMinute = wakeMinute;
    }

    public String getSongTitle() {
        return song.getTitle();
    }

    public String getWakeTime() {
        return String.format("%02d:%02d", wakeHour, wakeMinute);
    }

    public void activate(Context context){
        Intent intent = new Intent(context, AlarmReceiver.class);
        String songTitle = song.getTitle();
        Long id = song.getId();
        intent.putExtra(Alarm.SONG_TITLE_EXTRA, String.valueOf(songTitle));
        intent.putExtra(Alarm.SONG_ID_EXTRA, Long.valueOf(id));
        final int _id = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, _id, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        Calendar wakeTime = Calendar.getInstance();
        int currentHour = wakeTime.get(Calendar.HOUR_OF_DAY);
        int currentMinute = wakeTime.get(Calendar.MINUTE);
        if(wakeHour < currentHour || ( wakeHour == currentHour && wakeMinute <= currentMinute)){
            wakeTime.set(Calendar.DAY_OF_YEAR, wakeTime.get(Calendar.DAY_OF_YEAR) + 1);
        }
        wakeTime.set(Calendar.HOUR_OF_DAY, wakeHour);
        wakeTime.set(Calendar.MINUTE, wakeMinute);
        wakeTime.set(Calendar.SECOND, 0);
        wakeTime.set(Calendar.MILLISECOND, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, wakeTime.getTimeInMillis(), pendingIntent);
    }


    public static List<Alarm> generateFakeItems(List<Song> songs){
        ArrayList<Alarm> list = new ArrayList<>(10);
        for(int i = 0; i < songs.size(); i++){
            Alarm alarm;
            switch (i){
                case 13 : alarm = new Alarm(songs.get(i), 19, 31); break;
                case 14 : alarm = new Alarm(songs.get(i), 19, 31); break;
                default: alarm = new Alarm(songs.get(i), 0, 0);
            }
            list.add(alarm);
        }

        return list;
    }
}
