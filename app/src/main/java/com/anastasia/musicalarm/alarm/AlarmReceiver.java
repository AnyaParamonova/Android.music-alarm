package com.anastasia.musicalarm.alarm;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Anastasia_Paramonova on 19.04.2017.
 */

public class AlarmReceiver extends BroadcastReceiver {

    private static ArrayList<MediaPlayer> players = new ArrayList<>();

    @Override
    public void onReceive(Context context, Intent intent) {
        long id = intent.getLongExtra(Alarm.SONG_ID_EXTRA, 1);
        if(id != -1){
            Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id);

            MediaPlayer player = new MediaPlayer();
            players.add(player);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                player.setDataSource(context, uri);
                player.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
            PlayThread playThread = new PlayThread();
            playThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, player);
        }
        String songTitle = intent.getStringExtra(Alarm.SONG_TITLE_EXTRA);
        Toast.makeText(context, songTitle, Toast.LENGTH_LONG).show();

    }

    public static void stopAllAlarms(){
        for(MediaPlayer player : players){
            player.stop();
        }
        players = new ArrayList<>();
    }


    private class PlayThread extends AsyncTask<MediaPlayer, Void, Void> {
        @Override
        protected Void doInBackground(MediaPlayer... mediaPlayers) {
            mediaPlayers[0].start();
            return null;
        }
    }
}
