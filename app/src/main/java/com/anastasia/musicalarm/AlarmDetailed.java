package com.anastasia.musicalarm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.anastasia.musicalarm.adapter.RecyclerAdapter;

import java.util.ArrayList;

public class AlarmDetailed extends AppCompatActivity {

    private TextView wakeTime;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_detailed);

        wakeTime = (TextView) findViewById(R.id.detailed_wake_time);
        spinner = (Spinner) findViewById(R.id.spinner);

        String passedSongTitle = getIntent().getStringExtra(RecyclerAdapter.SONG_TITLE_EXTRA);
        String passedWakeTime = getIntent().getStringExtra(RecyclerAdapter.WAKE_TIME_EXTRA);
        ArrayList<String> songs = getIntent().getStringArrayListExtra(RecyclerAdapter.SONG_LIST_EXTRA);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AlarmDetailed.this,
                android.R.layout.simple_list_item_1, songs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        wakeTime.setText(passedWakeTime);
        spinner.setAdapter(adapter);
        spinner.setSelection(songs.indexOf(passedSongTitle));
    }
}
